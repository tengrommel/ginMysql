package apis

import (
	"github.com/gin-gonic/gin"
	"log"
	"fmt"
	"net/http"
	. "gitlab.com/tengrommel/ginMysql/models"
	"strconv"
)

func AddStuApi(c *gin.Context) {
	name := c.Request.FormValue("name")
	age := c.Request.FormValue("age")

	s := Stu{Name: name, Age: age}

	ra, err := s.AddStu()
	if err != nil {
		log.Fatalln(err)
	}
	msg := fmt.Sprintf("insert successful %d", ra)
	c.JSON(http.StatusOK, gin.H{
		"msg": msg,
	})
}

func GetStusApi(c *gin.Context) {
	var s Stu
	stus, err := s.GetStus()
	if err != nil {
		log.Fatalln(err)
	}

	c.JSON(http.StatusOK, gin.H{
		"stus": stus,
	})

}

func GetStuApi(c *gin.Context)  {
	cid := c.Param("id")
	id, err := strconv.Atoi(cid)
	if err != nil {
		log.Fatalln(err)
	}
	s := Stu{Id: id}
	stu, err := s.GetStu()
	if err != nil {
		log.Fatalln(err)
	}

	c.JSON(http.StatusOK, gin.H{
		"stu": stu,
	})
}

func ModStuApi(c *gin.Context) {
	cid := c.Param("id")
	id, err := strconv.Atoi(cid)
	if err != nil {
		log.Fatalln(err)
	}
	s := Stu{Id: id}
	err = c.Bind(&s)
	if err != nil {
		log.Fatalln(err)
	}
	ra, err := s.ModStu()
	if err != nil {
		log.Fatalln(err)
	}
	msg := fmt.Sprintf("Update stu %d successful %d", s.Id, ra)
	c.JSON(http.StatusOK, gin.H{
		"msg": msg,
	})
}

func DelStuApi(context *gin.Context)  {
	cid := context.Param("id")
	id, err := strconv.Atoi(cid)
	if err != nil {
		log.Fatalln(err)
	}
	s := Stu{Id: id}
	ra, err := s.DelStu()
	if err != nil {
		log.Fatalln(err)
	}
	msg := fmt.Sprintf("Delete stu %d successful %d", id, ra)
	context.JSON(http.StatusOK, gin.H{
		"msg": msg,
	})
}
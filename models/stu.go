package models

import (
	db "gitlab.com/tengrommel/ginMysql/database"
	"log"
)

type Stu struct {
	Id 					 int				`json:"id" form:"id"`
	Name    string			`json:"name" form:"name"`
	Age		 string     `json:"age" form:"age"`
}

func (s *Stu)AddStu() (id int64, err error) {
	rs, err := db.SqlDB.Exec("INSERT INTO stu(name, age) VALUES (?, ?)", s.Name, s.Age)
	if err != nil{
		log.Fatal(err)
		return
	}
	id , err = rs.LastInsertId()
	return
}

func (s *Stu)GetStus() (stus []Stu, err error) {
	stus = make([]Stu, 0)
	rows, err := db.SqlDB.Query("SELECT id, name, age FROM stu")
	defer rows.Close()

	if err != nil{
		log.Fatal(err)
		return
	}

	for rows.Next(){
		var stu Stu
		rows.Scan(&stu.Id, &stu.Name, &stu.Age)
		stus = append(stus, stu)
	}
	if err = rows.Err(); err != nil{
		return
	}
	return
}

func (s *Stu)GetStu() (stu Stu, err error)  {
	err = db.SqlDB.QueryRow("SELECT id, name, age FROM stu WHERE id=?", s.Id).Scan(
		&stu.Id, &stu.Name, &stu.Age,
	)
	return
}

func (s *Stu) ModStu() (ra int64, err error) {
	stmt, err := db.SqlDB.Prepare("UPDATE stu SET name=?, age=? WHERE id=?")
	defer stmt.Close()
	if err != nil {
		return
	}
	rs, err := stmt.Exec(s.Name, s.Age, s.Id)
	if err != nil {
		return
	}
	ra, err = rs.RowsAffected()
	return
}
func (stu *Stu) DelStu() (ra int64, err error) {
	rs, err := db.SqlDB.Exec("DELETE FROM stu WHERE id=?", stu.Id)
	if err != nil {
		log.Fatalln(err)
	}
	ra, err = rs.RowsAffected()
	return
}
package main

import (
	"github.com/gin-gonic/gin"
	. "gitlab.com/tengrommel/ginMysql/apis"
)

func initRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/", IndexApi)
	stu := router.Group("/api/stus")
	stu.GET("/", GetStusApi)
	stu.GET("/:id", GetStuApi)
	stu.PUT("/:id", ModStuApi)
	stu.DELETE("/:id", DelStuApi)
	stu.POST("/", AddStuApi)
	people := router.Group("/api/persons")
	people.GET("/", GetPersonsApi)
	people.POST("/", AddPersonApi)
	people.GET("/:id", GetPersonApi)
	people.PUT("/:id", ModPersonApi)
	people.DELETE("/:id", DelPersonApi)
	return router
}

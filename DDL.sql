-- auto-generated definition
CREATE TABLE person
(
  id         INT AUTO_INCREMENT
    PRIMARY KEY,
  first_name VARCHAR(40) DEFAULT '' NOT NULL,
  last_name  VARCHAR(40) DEFAULT '' NOT NULL
)
  ENGINE = InnoDB
  CHARSET = utf8;


-- auto-generated definition
CREATE TABLE stu
(
  id   INT(10) DEFAULT '0' NOT NULL
    PRIMARY KEY,
  name VARCHAR(20)         NULL,
  age  INT(10)             NULL
)
  ENGINE = InnoDB;